const app = angular.module('counterApp', []);

app.controller('counterController', ['$scope', ($scope) => {
    $scope.value = 0;
    $scope.decrement = () => {
        $scope.value = $scope.value - 1
    };
    $scope.increment = () => {
        $scope.value = $scope.value + 1
    };
}])